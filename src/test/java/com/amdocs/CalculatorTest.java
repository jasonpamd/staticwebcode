package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest{
    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int result = calculator.add();
        int expectedResult = 3 + 6;
        assertEquals("Add", expectedResult, result);
    }

    @Test
    public void testSub() {
        Calculator calculator = new Calculator();
        int result = calculator.sub();
        int expectedResult = 6 - 3;
        assertEquals("Sub", expectedResult, result);
    }

}
