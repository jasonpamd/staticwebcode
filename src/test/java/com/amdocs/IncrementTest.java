package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

    @Test
    public void decreasecounterGetCounterTest() {
       Increment increment = new Increment();
       int result = increment.getCounter();
       result = increment.getCounter();
       int expectedResult = 2;
       assertEquals(expectedResult, result);

    }
    @Test
    public void decreasecounterZeroTest() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(0);
        result = increment.decreasecounter(0);
	int expectedResult = 0;
        assertEquals(expectedResult, result);

    }
       
    @Test
    public void decreasecounterOneTest() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(1);
        result = increment.decreasecounter(1);
	int expectedResult = 1;
        assertEquals(expectedResult, result);

    }
    
    @Test
    public void decreasecounterOtherTest() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(2);
        result = increment.decreasecounter(2);
	int expectedResult = 2;
        assertEquals(expectedResult, result);
   }	
}
